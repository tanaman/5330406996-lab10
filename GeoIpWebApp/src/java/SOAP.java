import com.sun.xml.internal.messaging.saaj.soap.Envelope;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class SOAP {

    public static void main(String[] args) {
        try {
            SecurePass.install();
            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPEnvelope env = message.getSOAPPart().getEnvelope();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            String namespace = "https://rdws.rd.go.th/ServiceRD/CheckTINPINService";
            SOAPBodyElement servicePin = body.addBodyElement(env.createName("ServicePIN", null, namespace));
            SOAPElement username = servicePin.addChildElement(env.createName("username"));
            SOAPElement password = servicePin.addChildElement(env.createName("password"));
            SOAPElement pin = servicePin.addChildElement(env.createName("PIN"));
            username.addTextNode("anonymous");
            password.addTextNode("anonymous");
            pin.addTextNode("1103700582916");
            
            System.out.println("REQUEST:");
            displayMessage(message);
            System.out.println("\n\n");
            SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
            SOAPMessage response = conn.call(message, "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");
            System.out.println("RESPONSE:");
            displayMessage(response);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
     public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
}
